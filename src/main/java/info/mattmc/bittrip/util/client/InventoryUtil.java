package info.mattmc.bittrip.util.client;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;

public class InventoryUtil {
	/**
	 */
	public static void addItemToPlayerInventory(EntityPlayer player, Item item,
			boolean doesItemAlreadyExist) {
		if (doesItemAlreadyExist) {
			for (int i = 0; i < player.inventory.getSizeInventory(); i++) {
				if (player.inventory.getStackInSlot(i).equals(item)) {
					player.inventory
							.addItemStackToInventory(new ItemStack(item));
					return;
				}
			}
		}else if(!doesItemAlreadyExist){
			player.inventory.addItemStackToInventory(new ItemStack(item));
		}
	}
}
