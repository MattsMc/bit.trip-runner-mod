package info.mattmc.bittrip.util.client;

import info.mattmc.bittrip.lib.Reference;
import net.minecraft.entity.Entity;
import net.minecraft.world.World;

public class PlayHandler {
    /**
     * Play sounds around Entity
     * 
     * @param name
     * @param world
     * @param entity
     * @param volume
     * @param pitch
     */
    public static void playAtEntity(String name, World world, Entity entity,
            float volume, float pitch) {
        world.playSoundAtEntity(entity, Reference.MODID + ":" + name,
                (float) volume, (float) pitch);
    }
    
    
}
