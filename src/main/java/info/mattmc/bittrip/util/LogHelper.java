package info.mattmc.bittrip.util;

import info.mattmc.bittrip.lib.Reference;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class LogHelper {
    private static Logger logger = LogManager.getLogger(Reference.NAME);
    public static boolean loggerInitialized;
    
    /**
     * <i>Please use this method in the init/postInit, why would you make Minecraft do more work when it already has all those other things to load</i><br><br>
     * Initializes the logger
     * 
     * @author Matt
     */
    public static void init() {
        logger.log(Level.INFO, "Logger initialized.");
        loggerInitialized = true;
    }
    
    /**
     * Logs a message to the console <br>
     * <br>
     * org.apache.logging.log4j.Level
     * 
     * @author Matt
     * 
     * @category Logging
     * 
     * @param level
     * @param message
     */
    public static void log(Level level, String message) {
        if (loggerInitialized) logger.log(level, message);
        else System.out.println("[Severe] The Logger has not been initialized, Please do this before using it!");
    }
}
