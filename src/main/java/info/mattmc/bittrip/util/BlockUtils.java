package info.mattmc.bittrip.util;

import net.minecraft.world.World;

public class BlockUtils {
    /**
     * 
     * @param world
     * @param x
     * @param y
     * @param z
     * @param block
     */
    public static void setBlock(World world, int x, int y, int z,
            net.minecraft.block.Block block) {
        
        if (checkIfAir(world, x, y, z) || world.getBlock(x, y, z).isReplaceable(world, x, y, z)) world.setBlock(x, y, z, block);
    }
    
    /**
     * Removes a (specified) block from the world
     * @param world
     * @param x
     * @param y
     * @param z
     * @param block
     */
    public static void removeBlock(World world, int x, int y, int z, net.minecraft.block.Block block) {
        if(world.getBlock(x, y, z) == block){
        world.setBlockToAir(x, y, z);
        }
    }
    
     /**
      * 
      * @param world
      * @param x
      * @param y
      * @param z
      * @return
      */
    public static boolean checkIfAir(World world, int x, int y, int z){
        if (world.isAirBlock(x, y, z)){
            return true;
        }else {
            return false;
        }
    }
}
