package info.mattmc.bittrip.util;

import net.minecraft.entity.item.EntityItem;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.world.World;

public class ItemUtils {
    /**
     * Drops a item on the ground
     * @param world
     * @param x
     * @param y
     * @param z
     * @param item
     * @param amount
     */
    public static void dropItem(World world, int x, int y,int z, Item item, int amount){
        if(!world.isRemote){
            EntityItem entity = new EntityItem(world, x, y, z, new ItemStack(item, amount));
            world.spawnEntityInWorld(entity);
            
        }
    }
}
