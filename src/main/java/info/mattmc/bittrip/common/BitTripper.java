package info.mattmc.bittrip.common;

import info.mattmc.bittrip.command.BitTripCommand;
import info.mattmc.bittrip.event.EventListener;
import info.mattmc.bittrip.lib.Reference;
import info.mattmc.bittrip.proxy.CommonProxy;
import info.mattmc.bittrip.util.LogHelper;
import info.mattmc.bittrip.util.client.XMLParser;
import cpw.mods.fml.common.FMLCommonHandler;
import cpw.mods.fml.common.Mod;
import cpw.mods.fml.common.Mod.EventHandler;
import cpw.mods.fml.common.Mod.Instance;
import cpw.mods.fml.common.SidedProxy;
import cpw.mods.fml.common.event.FMLInitializationEvent;
import cpw.mods.fml.common.event.FMLPostInitializationEvent;
import cpw.mods.fml.common.event.FMLPreInitializationEvent;
import cpw.mods.fml.common.event.FMLServerStartingEvent;

@Mod(modid = Reference.MODID, version = Reference.VERSION, name = Reference.NAME)
public class BitTripper {
	@Instance(Reference.MODID)
	public static BitTripper instance;

	@SidedProxy(clientSide = "info.mattmc.bittrip.proxy.ClientProxy", serverSide = "info.mattmc.bittrip.proxy.CommonProxy")
	public static CommonProxy proxy;

	public static boolean isParkourActive;
	
	@EventHandler
	public void preInit(FMLPreInitializationEvent event) {
		// Load EventHandler Class
		FMLCommonHandler.instance().bus().register(new EventListener());

		// Load Items,Blocks and CreativeTab
		Stuff.load();

		// Load ClientRendering
		proxy.renderInformation();
		
		isParkourActive = false;
	}

	@EventHandler
	public void init(FMLInitializationEvent event) {
		LogHelper.init();
	}

	@EventHandler
	public void serverLoading(FMLServerStartingEvent event) {
		event.registerServerCommand(new BitTripCommand());
	}

	@EventHandler
	public void postInit(FMLPostInitializationEvent event) {
		XMLParser.checkUpdate(null, false, true, Reference.VERSIONCHECKURL,
				Reference.MCVERSION, Reference.MODID, Reference.VERSION, Reference.UPDATEURL);
	}
}
