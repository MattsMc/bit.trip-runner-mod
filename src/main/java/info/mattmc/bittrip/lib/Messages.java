package info.mattmc.bittrip.lib;

public class Messages {
    public static String CoreMessage = "[Bit.Trip Runner]";
    
    public static String CorrectVersion = "No Updates Found!";
    public static String UpdateAvalable = "Update Avalable!";
    public static String VersionCheckError = CoreMessage + " An Error Has Occured While Checking For Updates! Please contact the mod author";
    
    public static String IGUpdateFound = CoreMessage + " There Is A New Update Avalable for Bit.Trip Runner";
    
    public static String Error = CoreMessage + " An Error Has Occurred!";
    
    
}
