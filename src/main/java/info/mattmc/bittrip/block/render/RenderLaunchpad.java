package info.mattmc.bittrip.block.render;

import info.mattmc.bittrip.block.model.ModelLaunchpad;
import info.mattmc.bittrip.lib.Reference;
import net.minecraft.client.renderer.tileentity.TileEntitySpecialRenderer;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.ResourceLocation;

import org.lwjgl.opengl.GL11;

public class RenderLaunchpad extends TileEntitySpecialRenderer {
    private ModelLaunchpad                model;
    private static final ResourceLocation crystalTexure = new ResourceLocation(
                                                                Reference.MODID
                                                                        .toLowerCase(),
                                                                "textures/model/blockLaunchpad.png");
    
    public RenderLaunchpad() {
        this.model = new ModelLaunchpad();
    }
    
    public void renderTileEntityAt(TileEntity tile, double x, double y,
            double z, float f) {
        GL11.glPushMatrix(); // {
        GL11.glTranslatef((float) x + 0.5F, (float) y + 1.5F, (float) z + 0.5F); // dunno
        GL11.glRotatef(180F, 0F, 0F, 1F); //rotate
        bindTexture(crystalTexure);
        GL11.glPushMatrix(); // {
        model.renderModel(0.0625F); // Sets render of base
        GL11.glPopMatrix(); // }
        GL11.glPopMatrix(); // }
    }
    
}
