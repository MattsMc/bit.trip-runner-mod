package info.mattmc.bittrip.block;

import info.mattmc.bittrip.block.tile.TileEntityBlockStarter;
import info.mattmc.bittrip.common.Stuff;
import net.minecraft.block.BlockContainer;
import net.minecraft.block.material.Material;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.world.World;

public class BlockStarter extends BlockContainer {

	public BlockStarter() {
		super(Material.rock);
		setCreativeTab(Stuff.BitTrippingTab);
		setBlockName("blockStarter");
	}

	@Override
	public TileEntity createNewTileEntity(World var1, int var2) {
		return new TileEntityBlockStarter();
	}
	
	@Override
	public boolean onBlockActivated(World world, int x,
			int y, int z, EntityPlayer player,
			int hitX, float hitY, float hitZ,
			float f) {
		return false;
	}
	

}
