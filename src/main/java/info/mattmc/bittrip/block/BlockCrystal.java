package info.mattmc.bittrip.block;

import info.mattmc.bittrip.block.tile.BlockCrystalTile;
import info.mattmc.bittrip.common.Stuff;
import info.mattmc.bittrip.lib.Reference;
import net.minecraft.block.BlockContainer;
import net.minecraft.block.material.Material;
import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.world.World;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

public class BlockCrystal extends BlockContainer {
    
    public BlockCrystal() {
        super(Material.rock);
        System.out.println(1F / 16F * 1F);
        this.setBlockBounds(1F / 16F * 1F,
                0F,
                3F / 16F * 1F,
                1F - 1F / 16F * 3F,
                1F - 1F / 16F * 3F,
                1F - 1F / 16F * 3F);
        setBlockName("blockCrystalBlock");
        setCreativeTab(Stuff.BitTrippingTab);
        this.setHardness(2F);
    }
    
    @Override
    public TileEntity createNewTileEntity(World var1, int var2) {
        return new BlockCrystalTile();
    }
    
    public int getRenderType() {
        return -1;
    }
    
    public boolean isOpaqueCube() {
        return false;
    }
    
    @SideOnly(Side.CLIENT)
    public int getRenderBlockPass() {
        return 1;
    }

    public boolean renderAsNormalBlock() {
        return false;
    }
    
    @SideOnly(Side.CLIENT)
    public void registerBlockIcons(IIconRegister icon) {
        this.blockIcon = icon.registerIcon(Reference.MODID + ":"
                + this.getUnlocalizedName().substring(5));
    }
    
}
