package info.mattmc.bittrip.block;

import info.mattmc.bittrip.block.tile.BlockLaunchpadTile;
import info.mattmc.bittrip.common.Stuff;
import info.mattmc.bittrip.lib.Reference;
import net.minecraft.block.BlockContainer;
import net.minecraft.block.material.Material;
import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.entity.Entity;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.world.World;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

public class BlockLaunchpad extends BlockContainer {
    
    public BlockLaunchpad() {
        super(Material.carpet);
        this.setBlockName("blockLaunchpad");
        this.setCreativeTab(Stuff.BitTrippingTab);
        this.setBlockBounds(0.15F, 0F, 0.15F, 0.85F, 0.0625F, 0.85F);
    }
    
    public int getRenderType() {
        return -1;
    }
    
    public boolean isOpaqueCube() {
        return false;
    }
    
    @SideOnly(Side.CLIENT)
    public int getRenderBlockPass() {
        return 1;
    }

    public boolean renderAsNormalBlock() {
        return false;
    }
    
    public TileEntity createNewTileEntity(World var1, int var2) {
        return new BlockLaunchpadTile();
        
    }
    
    @SideOnly(Side.CLIENT)
    public void registerBlockIcons(IIconRegister icon) {
        this.blockIcon = icon.registerIcon(Reference.MODID + ":"
                + this.getUnlocalizedName().substring(5));
    }
    
    //NOTE: The launching works with look vectors, look from 2 blocks away (from the jump block) at the block you want to jump to.
    public void onEntityWalking(World world, int x, int y, int z, Entity entity) {
        entity.setVelocity(entity.getLookVec().xCoord, entity.getLookVec().yCoord+1, entity.getLookVec().zCoord);
        
    }
    
}
