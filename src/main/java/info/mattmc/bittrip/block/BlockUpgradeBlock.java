package info.mattmc.bittrip.block;

import info.mattmc.bittrip.common.Stuff;
import info.mattmc.bittrip.lib.Reference;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.world.World;

public class BlockUpgradeBlock extends Block {
    public BlockUpgradeBlock(int i) {
        super(Material.rock);
        this.setBlockName("blockUpgradeBlock");
        this.setBlockTextureName(Reference.MODID + ":blockUpgradeBlock");
        this.setCreativeTab(Stuff.BitTrippingTab);
    }
    
    @Override
    public boolean onBlockActivated(World world, int x, int y, int z,
            EntityPlayer player, int p_149727_6_, float p_149727_7_,
            float p_149727_8_, float p_149727_9_) {
        
        if (!world.isRemote) {
//            BlockUtils.removeBlock(world, x, y, z, Stuff.blockUpgradeBlock);
//            EntityItemUpgrade entityitemupgrade = new EntityItemUpgrade(world,
//                    x, y, z);
//            world.spawnEntityInWorld(entityitemupgrade);
        }
        
        return super.onBlockActivated(world, x, y, z, player, p_149727_6_,
                p_149727_7_, p_149727_8_, p_149727_9_);
    }
}
