package info.mattmc.bittrip.proxy;

import info.mattmc.bittrip.block.render.ItemRenderCrystal;
import info.mattmc.bittrip.block.render.ItemRenderLaunchpad;
import info.mattmc.bittrip.block.render.RenderCrystal;
import info.mattmc.bittrip.block.render.RenderLaunchpad;
import info.mattmc.bittrip.block.tile.BlockCrystalTile;
import info.mattmc.bittrip.block.tile.BlockLaunchpadTile;
import info.mattmc.bittrip.common.Stuff;
import info.mattmc.bittrip.item.entity.EntityItemGoldy;
import info.mattmc.bittrip.item.entity.EntityItemUpgrade;
import info.mattmc.bittrip.item.entity.render.RenderItemGoldy;
import info.mattmc.bittrip.item.entity.render.RenderItemUpgrade;
import info.mattmc.bittrip.util.client.ClientRegistryHelper;
import cpw.mods.fml.client.registry.ClientRegistry;
import cpw.mods.fml.client.registry.RenderingRegistry;

public class ClientProxy extends CommonProxy {
    
    @Override
    public void renderInformation() {
        ClientRegistry.bindTileEntitySpecialRenderer(BlockCrystalTile.class, new RenderCrystal());
        ClientRegistry.bindTileEntitySpecialRenderer(BlockLaunchpadTile.class, new RenderLaunchpad());
        
        ClientRegistryHelper.registerItemRenderer(Stuff.blockCrystal, new ItemRenderCrystal());
        ClientRegistryHelper.registerItemRenderer(Stuff.blockLaunchpad, new ItemRenderLaunchpad());
        
        RenderingRegistry.registerEntityRenderingHandler(EntityItemGoldy.class, new RenderItemGoldy());
        RenderingRegistry.registerEntityRenderingHandler(EntityItemUpgrade.class, new RenderItemUpgrade());
        
    }
    
 
    
}
