package info.mattmc.bittrip.item;

import info.mattmc.bittrip.common.Stuff;
import info.mattmc.bittrip.lib.Reference;
import net.minecraft.item.Item;

public class ItemUpgrade extends Item {
    public ItemUpgrade() {
        super();
        this.setUnlocalizedName("itemUpgrade");
        setCreativeTab(Stuff.BitTrippingTab);
        this.setTextureName(Reference.MODID + ":itemUpgrade");
    }
}
