package info.mattmc.bittrip.item;

import info.mattmc.bittrip.common.Stuff;
import info.mattmc.bittrip.lib.Reference;

import java.util.List;

import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.AxisAlignedBB;
import net.minecraft.util.StatCollector;
import net.minecraft.world.World;

public class ItemRangeRemover extends Item {

	public ItemRangeRemover() {
		super();
		this.setUnlocalizedName("itemRangeRemover");
		setCreativeTab(Stuff.BitTrippingTab);
		this.setTextureName(Reference.MODID + ":itemRangeRemover");
	}

	@Override
	public void addInformation(ItemStack par1ItemStack,
			EntityPlayer par2EntityPlayer, List par3List, boolean par4) {
		par3List.add(StatCollector
				.translateToLocal("item.itemRangeRemover.desc1"));
		par3List.add(StatCollector
				.translateToLocal("item.itemRangeRemover.desc2"));
		super.addInformation(par1ItemStack, par2EntityPlayer, par3List, par4);
	}

	@Override
	public ItemStack onItemRightClick(ItemStack par1ItemStack, World par2World,
			EntityPlayer par3EntityPlayer) {
		List<Entity> entitys = par2World.getEntitiesWithinAABB(Entity.class,
				AxisAlignedBB.getBoundingBox(par3EntityPlayer.posX - 2.5,
						par3EntityPlayer.posY - 2.5,
						par3EntityPlayer.posZ - 2.5,
						par3EntityPlayer.posX + 2.5,
						par3EntityPlayer.posY + 2.5,
						par3EntityPlayer.posZ + 2.5));

		for (int i = 0; i < entitys.size(); i++) {
			if (!(entitys.get(i).getEntityId() == 2516)) {
				if (entitys.get(i).getCommandSenderName()
						.equals("entity.EntityItemGoldy.name")
						| entitys.get(i).getCommandSenderName()
								.equals("entity.EntityItemUpgrade.name")) {
					par2World.removeEntity(entitys.get(i));
				}
			}
		}
		return super.onItemRightClick(par1ItemStack, par2World,
				par3EntityPlayer);
	}

}
