package info.mattmc.bittrip.item;

import info.mattmc.bittrip.common.BitTripper;
import info.mattmc.bittrip.common.Stuff;
import info.mattmc.bittrip.item.entity.EntityItemGoldy;
import info.mattmc.bittrip.item.entity.EntityItemUpgrade;
import info.mattmc.bittrip.util.BlockUtils;

import java.util.ArrayList;
import java.util.List;

import net.minecraft.block.Block;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.AxisAlignedBB;
import net.minecraft.util.StatCollector;
import net.minecraft.world.World;

public class ItemStarter extends Item {
	int amountBlocks = 0;
	
	public ItemStarter() {
		super();
		this.setNoRepair();
		this.setCreativeTab(Stuff.BitTrippingTab);
		this.setUnlocalizedName("itemStarterDeActive");
	}

	ArrayList blockUpgrade = new ArrayList();
	ArrayList blockGoldy = new ArrayList();

	@Override
	public void addInformation(ItemStack par1ItemStack,
			EntityPlayer par2EntityPlayer, List par3List, boolean par4) {
		par3List.add(StatCollector.translateToLocal("item.itemStarter.desc1"));
		par3List.add(StatCollector.translateToLocal("item.itemStarter.desc2"));
		super.addInformation(par1ItemStack, par2EntityPlayer, par3List, par4);
	}

	public ItemStack onItemRightClick(ItemStack itemStack, World world,
			EntityPlayer player) {
		if (!world.isRemote) {
			if (player.isSneaking()) {
				if (this.getUnlocalizedName()
						.equals("item.itemStarterDeActive")) {
					this.setUnlocalizedName("itemStarterActive");
					BitTripper.isParkourActive = true;
					if (blockUpgrade.size() >= 1) {
						for (int i = 0; i < blockUpgrade.size(); i++) {
							String[] coords = blockUpgrade.get(i).toString()
									.split(",");
							int x = Integer.parseInt(coords[0]);
							int y = Integer.parseInt(coords[1]);
							int z = Integer.parseInt(coords[2]);
							System.out.println(x + "," + y + "," + z);
							EntityItemUpgrade itemUpgrade = new EntityItemUpgrade(
									world, x, y, z);
							replaceBlockByEntity(world, itemUpgrade, x, y, z,
									Stuff.blockUpgradeBlock);
						}
					}
					if (blockGoldy.size() >= 1) {
						for (int i = 0; i < blockGoldy.size(); i++) {
							String[] coords = blockGoldy.get(i).toString()
									.split(",");
							int x = Integer.parseInt(coords[0]);
							int y = Integer.parseInt(coords[1]);
							int z = Integer.parseInt(coords[2]);
							System.out.println(x + "," + y + "," + z);
							EntityItemGoldy itemGoldy = new EntityItemGoldy(
									world, x, y, z);
							replaceBlockByEntity(world, itemGoldy, x, y, z,
									Stuff.blockItemGold);
						}
					}
				} else {
					BitTripper.isParkourActive = false;
					this.setUnlocalizedName("itemStarterDeActive");
					if (blockUpgrade.size() >= 1) {
						for (int i = 0; i < blockUpgrade.size(); i++) {
							String[] coords = blockUpgrade.get(i).toString()
									.split(",");
							int x = Integer.parseInt(coords[0]);
							int y = Integer.parseInt(coords[1]);
							int z = Integer.parseInt(coords[2]);
							System.out.println(x + "," + y + "," + z);
							List<Entity> entitys = world.getEntitiesWithinAABB(
									Entity.class, AxisAlignedBB.getBoundingBox(
											x - 1, y - 1, z - 1, x + 1, y + 1,
											z + 1));

							for (int k = 0; k < entitys.size(); k++) {
								if (!(entitys.get(k).getEntityId() == 2516)) {
									world.removeEntity(entitys.get(k));
									world.setBlock(x, y, z,
											Stuff.blockUpgradeBlock);
									blockUpgrade.remove(k);
								}
							}
						}
					}
					if (blockGoldy.size() >= 1) {
						for (int i = 0; i < blockGoldy.size(); i++) {
							String[] coords = blockGoldy.get(i).toString()
									.split(",");
							int x = Integer.parseInt(coords[0]);
							int y = Integer.parseInt(coords[1]);
							int z = Integer.parseInt(coords[2]);
							List<Entity> entitys = world.getEntitiesWithinAABB(
									Entity.class, AxisAlignedBB.getBoundingBox(
											x - 1, y - 1, z - 1, x + 1, y + 1,
											z + 1));

							for (int k = 0; k < entitys.size(); k++) {
								if (!(entitys.get(k).getEntityId() == 2516)) {
									world.removeEntity(entitys.get(k));
									world.setBlock(x, y, z, Stuff.blockItemGold);
									blockGoldy.remove(k);
								}
							}
						}
					}
				}
			} else {
			}
		}
		return itemStack;
	}

	public void replaceBlockByEntity(World world, Entity entity, int x, int y,
			int z, Block block) {
		if (!world.isRemote) {
			BlockUtils.removeBlock(world, x, y, z, block);
			world.spawnEntityInWorld(entity);
		}
	}

	public void replaceEntityByBlock(World world, Entity entity, int x, int y,
			int z, Block block) {
		if (!world.isRemote) {
			BlockUtils.setBlock(world, x, y, z, block);
			world.removeEntity(entity);
		}
	}

	@Override
	public boolean onItemUseFirst(ItemStack stack, EntityPlayer player,
			World world, int x, int y, int z, int side, float hitX, float hitY,
			float hitZ) {
		if (!world.isRemote) {
			if (!BitTripper.isParkourActive) {
				if (world.getBlock(x, y, z) == Stuff.blockUpgradeBlock) {
					blockUpgrade.add(x + "," + y + "," + z);
				}
				if (world.getBlock(x, y, z) == Stuff.blockItemGold) {
					blockGoldy.add(x + "," + y + "," + z);
				}
			}
		}
		return super.onItemUseFirst(stack, player, world, x, y, z, side, hitX,
				hitY, hitZ);
	}

}
