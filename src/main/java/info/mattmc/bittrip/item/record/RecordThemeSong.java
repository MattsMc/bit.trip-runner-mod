package info.mattmc.bittrip.item.record;

import info.mattmc.bittrip.common.Stuff;
import info.mattmc.bittrip.lib.Reference;
import net.minecraft.item.EnumRarity;
import net.minecraft.item.ItemRecord;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ResourceLocation;

public class RecordThemeSong extends ItemRecord {

	public RecordThemeSong() {
		super("ThemeSong");
		this.setUnlocalizedName("Bit.TrippingThemeSong");
		this.setTextureName("record_11");
		this.setCreativeTab(Stuff.BitTrippingTab);
	}

	@Override
	public ResourceLocation getRecordResource(String name) {
		return new ResourceLocation(Reference.MODID.toLowerCase(), "BTRTS");
	}
	
    public EnumRarity getRarity(ItemStack par1ItemStack)
    {
        return EnumRarity.uncommon;
    }
}
