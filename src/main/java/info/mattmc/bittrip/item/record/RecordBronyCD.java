package info.mattmc.bittrip.item.record;

import info.mattmc.bittrip.common.Stuff;
import info.mattmc.bittrip.lib.Reference;
import net.minecraft.item.EnumRarity;
import net.minecraft.item.ItemRecord;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ResourceLocation;

public class RecordBronyCD extends ItemRecord {

	public RecordBronyCD() {
		super("BronyCD");
		this.setUnlocalizedName("BronyCD");
		this.setTextureName(Reference.MODID + ":recordBronyCD.png");
		this.setCreativeTab(Stuff.BitTrippingTab);
	}
    public EnumRarity getRarity(ItemStack par1ItemStack)
    {
        return EnumRarity.epic;
    }
    
    @Override
    public ResourceLocation getRecordResource(String name) {
    	
    	return new ResourceLocation(Reference.MODID.toLowerCase(), "MLPTS");
    }

}
