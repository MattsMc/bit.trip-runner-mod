package info.mattmc.bittrip.item.entity.render;

import info.mattmc.bittrip.item.entity.model.ModelItemGoldy;
import info.mattmc.bittrip.lib.Reference;
import net.minecraft.client.renderer.entity.Render;
import net.minecraft.entity.Entity;
import net.minecraft.util.ResourceLocation;

import org.lwjgl.opengl.GL11;

public class RenderItemGoldy extends Render {
    
    private static final ResourceLocation itemGoldyTexture = new ResourceLocation(
                                                                   Reference.MODID
                                                                           .toLowerCase(),
                                                                   "textures/model/itemGoldyModel.png");
    
    ModelItemGoldy                        model;
    
    public RenderItemGoldy() {
        model = new ModelItemGoldy();
    }
    
    @Override
    public void doRender(Entity var1, double var2, double var4, double var6,
            float var8, float var9) {
        GL11.glPushMatrix(); // {
        GL11.glTranslatef((float) var2 + 0.5F, (float) var4 + 1.5F, (float) var6 + 0.5F);
        GL11.glRotatef(180F, 0F, 0F, 1F); // rotate
        this.bindTexture(itemGoldyTexture); // set Texture
        GL11.glPushMatrix(); // {
        model.renderModel(0.0625F);
        GL11.glPopMatrix(); // }
        GL11.glPopMatrix(); // }
    }
    
    @Override
    protected ResourceLocation getEntityTexture(Entity var1) {
        return itemGoldyTexture;
    }
    
}
