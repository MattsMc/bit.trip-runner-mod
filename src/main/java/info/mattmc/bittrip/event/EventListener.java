package info.mattmc.bittrip.event;

import info.mattmc.bittrip.lib.Reference;
import info.mattmc.bittrip.util.client.PlayHandler;
import info.mattmc.bittrip.util.client.SendMessage;
import info.mattmc.bittrip.util.client.XMLParser;
import net.minecraft.init.Items;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagList;
import net.minecraft.nbt.NBTTagString;
import cpw.mods.fml.common.eventhandler.SubscribeEvent;
import cpw.mods.fml.common.gameevent.PlayerEvent.PlayerLoggedInEvent;

public class EventListener {
	@SubscribeEvent
	public void onPlayerLogin(PlayerLoggedInEvent event) {
		XMLParser.checkUpdate(event.player, false, false,
				Reference.VERSIONCHECKURL, Reference.MCVERSION,
				Reference.MODID, Reference.VERSION, Reference.UPDATEURL);

		if (event.player.getDisplayName().equals("JustKayneNL")) {
			PlayHandler.playAtEntity("MLPTS", event.player.worldObj,
					event.player, 1, 1);
		}
		SendMessage.sendPMP(event.player,
				"Welcome to the Bit.Trip Runner Open Beta!");
		SendMessage
				.sendPMP(
						event.player,
						"Please submit any bugs/Recommendations to the issue tracker! https://tinyurl.com/Bit-Trip-Issue");
				event.player.inventory.addItemStackToInventory(manual);
	}

	public static ItemStack manual = new ItemStack(Items.written_book);
	static {
		manual.setTagInfo("author", new NBTTagString("MattsMc"));
		manual.setTagInfo("title", new NBTTagString(
				"Bit.Trip Runner Beta Manual"));
		NBTTagList pages = new NBTTagList();
		pages.appendTag(new NBTTagString(
				"Hello and welcome to the bit.trip runner open beta! Please submit any bugs you find the the issue tracker. https://tinyurl.com/Bit-Trip-Issue"));
		pages.appendTag(new NBTTagString(
				"How to use the starter item: place goldy blocks and upgrade blocks in the world and bind them with the starter by rightclicking them."));
		pages.appendTag(new NBTTagString(
				"How to start the game: Shift-Rightclick the starter item and it will change all the blocks you clicked into items..."));
		pages.appendTag(new NBTTagString(
				"Credits: Code & (Most of textures) MattsMc (TheMcExpress) Songs: Game: Bit.Trip Runner (WiiWare) Music: Main Menu."));
		manual.setTagInfo("pages", pages);
	}
}
