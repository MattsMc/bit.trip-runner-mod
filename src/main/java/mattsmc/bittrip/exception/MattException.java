package mattsmc.bittrip.exception;

public class MattException extends Exception {
    public MattException() {
        super();
    }

    public MattException(String message) {
        super(message);
    }

    public MattException(String message, Throwable cause) {
        super(message, cause);
    }

    public MattException(Throwable cause) {
        super(cause);
    }

    protected MattException(String message, Throwable cause,
                        boolean enableSuppression,
                        boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
