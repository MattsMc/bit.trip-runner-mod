package mattsmc.bittrip.exception;

public class NotLikedException extends Exception {
    
    /**
     * Thrown if you are not liked!
     */
    public NotLikedException(){
        super();
    }
    
    public NotLikedException(String message) {
        super(message);
    }
    
    public NotLikedException(String message, Throwable cause) {
        super(message, cause);
    }
    
    public NotLikedException(Throwable cause) {
        super(cause);
    }
    
    
    
}
