package mattsmc.bittrip.util;

import com.google.common.annotations.Beta;

import mattsmc.bittrip.common.BitTripper;
import mattsmc.bittrip.lib.Reference;
import net.minecraft.block.Block;
import net.minecraft.entity.Entity;
import net.minecraft.item.Item;
import net.minecraft.tileentity.TileEntity;
import cpw.mods.fml.common.Optional.Method;
import cpw.mods.fml.common.registry.EntityRegistry;
import cpw.mods.fml.common.registry.GameRegistry;

public class RegistryHelper {
    
    /**
     * Registers a block with FML
     * 
     * @param block
     * 
     * @author Matt
     */
    @Deprecated
    public static void registerBlock(Block block, String name) {
        GameRegistry.registerBlock(block, Reference.MODID + "_"
                + name);
    }
    
    /**
     * Registers a block with FML
     * 
     * @param block
     * 
     * @author Matt
     */
    
    public static void registerBlock(Block block) {
        GameRegistry.registerBlock(block, Reference.MODID + "_"
                + block.getUnlocalizedName().substring(5));
    }
    
    /**
     * Registers a item with FML
     * 
     * @param item
     * 
     * @author Matt
     */
    public static void registerItem(Item item) {
        GameRegistry.registerItem(item, Reference.MODID + "_"
                + item.getUnlocalizedName().substring(5));
    }
    
    /**
     * Registers a tileEntity with FML
     * 
     * @param classy
     * 
     * @author Matt
     */
    public static void registerTileEntity(Class<?> classy) {
        GameRegistry.registerTileEntity((Class<? extends TileEntity>) classy,
                Reference.MODID + "_"
                        + classy.getName().toString().toLowerCase());
    }
    
    /**
     * Registers a Entity with FML
     * 
     * @param entityClass
     * @param entityName
     * @param id
     * @param trackingRange
     * 
     * @author Matt
     */
    public static void registerEntity(Class<? extends Entity> entityClass, String entityName, int id, int trackingRange) {
        EntityRegistry.registerGlobalEntityID(entityClass, entityName,
                EntityRegistry.findGlobalUniqueEntityId());
        EntityRegistry.registerModEntity(entityClass, entityName, id,
                BitTripper.instance, trackingRange, 1, false);
    }
    
    
}
