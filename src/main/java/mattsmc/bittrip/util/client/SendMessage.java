package mattsmc.bittrip.util.client;

import net.minecraft.command.ICommandSender;
import net.minecraft.util.ChatComponentText;

public class SendMessage {
    /**
     * Sends Personal Message to a Player
     * 
     * @param player
     * @param message
     */
    public static void sendPMP(ICommandSender player, String message) {
        player.addChatMessage(new ChatComponentText(message));
    }
}
