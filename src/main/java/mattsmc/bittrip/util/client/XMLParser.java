package mattsmc.bittrip.util.client;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import mattsmc.bittrip.lib.Messages;
import mattsmc.bittrip.lib.Reference;
import mattsmc.bittrip.util.LogHelper;
import net.minecraft.command.ICommandSender;

import org.apache.logging.log4j.Level;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class XMLParser extends Thread {

	private ICommandSender sender;

	private boolean sendNoUpdateMessage;
	private boolean printUpdateMessage;

	public static String URL = null;
	public static String MCVERSION = null;
	public static String MODID = null;
	public static String Version = null;
	public static String updateURL = null;

	public static String RESULT = null;

	private XMLParser(ICommandSender sender, boolean sendNoUpdateMessage,
			boolean printUpdateMessage, String URL, String MCVERSION,
			String MODID, String Version, String updateURL) {
		this.sender = sender;
		this.sendNoUpdateMessage = sendNoUpdateMessage;
		this.printUpdateMessage = printUpdateMessage;
		XMLParser.URL = URL;
		XMLParser.MCVERSION = MCVERSION;
		XMLParser.MODID = MODID;
		XMLParser.Version = Version;
		XMLParser.updateURL = updateURL;

	}

	/**
	 * Checks if there is a new update Available
	 * 
	 * @author Matt & Aroma1997
	 * 
	 * @since build 1
	 * 
	 * @param sender
	 * @param sendNoUpdateMessage
	 * @param printUpdateMessage
	 * @param URL
	 * @param MCVERSION
	 * @param MODID
	 * @param Version
	 * 
	 * @category Version Checking
	 */
	public static void checkUpdate(ICommandSender sender,
			boolean sendNoUpdateMessage, boolean printUpdateMessage,
			String URL, String MCVERSION, String MODID, String Version,
			String updateURL) {
		new XMLParser(sender, sendNoUpdateMessage, printUpdateMessage, URL,
				MCVERSION, MODID, Version, updateURL).start();
	}

	@Override
	public void run() {
		try {
			DocumentBuilderFactory f = DocumentBuilderFactory.newInstance();
			DocumentBuilder b = f.newDocumentBuilder();
			Document doc = b.parse(URL);

			doc.getDocumentElement().normalize();

			NodeList items = doc.getElementsByTagName("VersionCheck");
			for (int i = 0; i < items.getLength(); i++) {
				Node n = items.item(i);
				if (n.getNodeType() != Node.ELEMENT_NODE)
					continue;
				Element e = (Element) n;

				NodeList titleList = e.getElementsByTagName(MCVERSION);
				Element titleElem = (Element) titleList.item(0);

				Node titleNode = titleElem.getChildNodes().item(0);
				if (titleElem.getAttribute("modid").equals(MODID)) {
					if (titleNode.getNodeValue().equals(Version)) {
						if (printUpdateMessage != false) {
							LogHelper.log(Level.INFO, Messages.CorrectVersion
									+ " (" + titleNode.getNodeValue() + ")");
						}
						RESULT = "NoUpdate";
					} else {
						if (printUpdateMessage != false) {
							LogHelper.log(
									Level.INFO,
									Messages.UpdateAvalable
											+ " \nCurrent Version: " + Version
											+ ", New Version: "
											+ titleNode.getNodeValue());
						}
						RESULT = titleNode.getNodeValue();
					}
				} else {
					i++;
				}
			}
			if (sender != null) {
				if (RESULT != null) {
					if (RESULT != "error") {
						if (RESULT != "NoUpdate") {
							SendMessage.sendPMP(sender, Messages.IGUpdateFound);
							SendMessage.sendPMP(sender, " (Current Version: "
									+ Reference.VERSION + ", New Version: "
									+ RESULT + "), " + updateURL);
						} else {
							if (sendNoUpdateMessage == true) {
								SendMessage.sendPMP(sender,
										Messages.CorrectVersion);
							}
						}
					} else {
						SendMessage.sendPMP(sender, Messages.VersionCheckError);
					}
				}
			}
		} catch (Exception e) {
			RESULT = "error";
		} finally {
			System.gc();
		}
	}
}
