package mattsmc.bittrip.util.client;

import mattsmc.bittrip.common.Stuff;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;

public class InventoryUtil {
    public static void addItemToPlayerInventory(EntityPlayer player, Item item) {
            player.inventory.addItemStackToInventory(new ItemStack(item));
    }
}
