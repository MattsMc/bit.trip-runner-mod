package mattsmc.bittrip.util.client;

import net.minecraft.block.Block;
import net.minecraft.item.Item;
import net.minecraftforge.client.IItemRenderer;
import net.minecraftforge.client.MinecraftForgeClient;

public class ClientRegistryHelper {
    
    public static void registerItemRenderer(Block item, IItemRenderer classy){
        MinecraftForgeClient.registerItemRenderer(Item.getItemFromBlock(item), classy);
    }
    
}
