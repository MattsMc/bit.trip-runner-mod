package mattsmc.bittrip.item.entity.model;

import net.minecraft.client.model.ModelBase;
import net.minecraft.client.model.ModelRenderer;
import net.minecraft.entity.Entity;

public class ModelItemUpgrade extends ModelBase
{
  //fields
    ModelRenderer base;
    ModelRenderer pointy1;
    ModelRenderer pointy2;
    ModelRenderer pointy3;
    ModelRenderer pointy4;
    ModelRenderer pointy5;
    ModelRenderer pointy6;
  
  public ModelItemUpgrade()
  {
    textureWidth = 64;
    textureHeight = 32;
    
      base = new ModelRenderer(this, 0, 0);
      base.addBox(-1F, 0F, -1F, 6, 6, 6);
      base.setRotationPoint(-1F, 11F, -1F);
      base.setTextureSize(64, 32);
      base.mirror = true;
      setRotation(base, 0F, 0F, 0F);
      pointy1 = new ModelRenderer(this, 24, 6);
      pointy1.addBox(0F, 0F, 0F, 3, 3, 3);
      pointy1.setRotationPoint(4F, 12.5F, -0.5F);
      pointy1.setTextureSize(64, 32);
      pointy1.mirror = true;
      setRotation(pointy1, 0F, 0F, 0F);
      pointy2 = new ModelRenderer(this, 24, 0);
      pointy2.addBox(0F, 0F, 0F, 3, 3, 3);
      pointy2.setRotationPoint(-0.5F, 12.5F, 4F);
      pointy2.setTextureSize(64, 32);
      pointy2.mirror = true;
      setRotation(pointy2, 0F, 0F, 0F);
      pointy3 = new ModelRenderer(this, 36, 6);
      pointy3.addBox(0F, 0F, 0F, 3, 3, 3);
      pointy3.setRotationPoint(-0.5F, 12.5F, -5F);
      pointy3.setTextureSize(64, 32);
      pointy3.mirror = true;
      setRotation(pointy3, 0F, 0F, 0F);
      pointy4 = new ModelRenderer(this, 24, 12);
      pointy4.addBox(0F, 0F, 0F, 3, 3, 3);
      pointy4.setRotationPoint(-5F, 12.5F, -0.5F);
      pointy4.setTextureSize(64, 32);
      pointy4.mirror = true;
      setRotation(pointy4, 0F, 0F, 0F);
      pointy5 = new ModelRenderer(this, 36, 0);
      pointy5.addBox(0F, 0F, 0F, 3, 3, 3);
      pointy5.setRotationPoint(-0.5F, 8F, -0.5F);
      pointy5.setTextureSize(64, 32);
      pointy5.mirror = true;
      setRotation(pointy5, 0F, 0F, 0F);
      pointy6 = new ModelRenderer(this, 36, 12);
      pointy6.addBox(0F, 0F, 0F, 3, 3, 3);
      pointy6.setRotationPoint(-0.5F, 17F, -0.5F);
      pointy6.setTextureSize(64, 32);
      pointy6.mirror = true;
      setRotation(pointy6, 0F, 0F, 0F);
  }
  
  public void render(Entity entity, float f, float f1, float f2, float f3, float f4, float f5)
  {
    super.render(entity, f, f1, f2, f3, f4, f5);
    setRotationAngles(f, f1, f2, f3, f4, f5, entity);
    base.render(f5);
    pointy1.render(f5);
    pointy2.render(f5);
    pointy3.render(f5);
    pointy4.render(f5);
    pointy5.render(f5);
    pointy6.render(f5);
  }
  
  public void renderModel(float f5){
      base.render(f5);
      pointy1.render(f5);
      pointy2.render(f5);
      pointy3.render(f5);
      pointy4.render(f5);
      pointy5.render(f5);
      pointy6.render(f5);
  }
  
  private void setRotation(ModelRenderer model, float x, float y, float z)
  {
    model.rotateAngleX = x;
    model.rotateAngleY = y;
    model.rotateAngleZ = z;
  }
  
  public void setRotationAngles(float f, float f1, float f2, float f3, float f4, float f5, Entity entity)
  {
    super.setRotationAngles(f, f1, f2, f3, f4, f5, entity);
  }

}
