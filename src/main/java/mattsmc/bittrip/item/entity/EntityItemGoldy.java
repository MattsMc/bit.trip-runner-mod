package mattsmc.bittrip.item.entity;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.entity.Entity;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.world.World;

public class EntityItemGoldy extends Entity {
    Thread thread;

    public EntityItemGoldy(World par1World) {
        super(par1World);
        this.setSize(0.5F, 1F);
        thread = new Thread("itemGoldy");
    }
    
    public EntityItemGoldy(World par1World, double par2, double par4, double par6)
    {
        super(par1World);
        this.setPosition(par2, par4, par6);
        this.setSize(0.5F, 1F);
        this.yOffset = 0.0F;
    }

    @SideOnly(Side.CLIENT)
    public int getBrightnessForRender(float par1)
    {
        return 15728880;
    }
    
    public float getBrightness(float par1)
    {
        return 1.0F;
    }
    
    protected void entityInit() {
    }

    @Override
    protected void readEntityFromNBT(NBTTagCompound var1) {
    }

    @Override
    protected void writeEntityToNBT(NBTTagCompound var1) {
    }
    
}
