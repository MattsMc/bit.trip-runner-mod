package mattsmc.bittrip.item;

import mattsmc.bittrip.common.Stuff;
import mattsmc.bittrip.item.entity.EntityItemGoldy;
import mattsmc.bittrip.item.entity.EntityItemUpgrade;
import mattsmc.bittrip.lib.Reference;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Blocks;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.world.World;

public class ItemUpgrade extends Item {
    public ItemUpgrade() {
        super();
        this.setUnlocalizedName("itemUpgrade");
        setCreativeTab(Stuff.BitTrippingTab);
        this.setTextureName(Reference.MODID + ":itemUpgrade");
    }
}
