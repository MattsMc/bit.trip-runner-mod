package mattsmc.bittrip.item;

import mattsmc.bittrip.common.Stuff;
import mattsmc.bittrip.item.entity.EntityItemGoldy;
import mattsmc.bittrip.lib.Reference;
import net.minecraft.block.Block;
import net.minecraft.block.BlockEndPortalFrame;
import net.minecraft.entity.item.EntityEnderEye;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Blocks;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.Direction;
import net.minecraft.util.MovingObjectPosition;
import net.minecraft.world.ChunkPosition;
import net.minecraft.world.World;

public class ItemGoldy extends Item {
    public ItemGoldy() {
        super();
        this.setUnlocalizedName("itemGoldy");
        setCreativeTab(Stuff.BitTrippingTab);
        this.setTextureName(Reference.MODID + ":itemGoldy");
    }
    
    
    
}
