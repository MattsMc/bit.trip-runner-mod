package mattsmc.bittrip.item;

import java.util.List;

import mattsmc.bittrip.common.Stuff;
import mattsmc.bittrip.lib.Reference;
import mattsmc.bittrip.util.WorldUtil;
import mattsmc.bittrip.util.client.InventoryUtil;
import mattsmc.bittrip.util.client.PlayHandler;
import net.minecraft.client.audio.SoundHandler;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.AxisAlignedBB;
import net.minecraft.util.StatCollector;
import net.minecraft.world.World;

public class ItemActivator extends Item {
	public ItemActivator() {
		super();
		this.setCreativeTab(Stuff.BitTrippingTab);
		this.setUnlocalizedName("itemActivator");
		this.setTextureName(Reference.MODID + ":itemActivator");
	}

	@Override
	public void addInformation(ItemStack par1ItemStack, EntityPlayer player,
			List list, boolean par4) {
		list.add(StatCollector.translateToLocal("item.itemActivator.desc1"));
		list.add(StatCollector.translateToLocal("item.itemActivator.desc2"));
	}

	@Override
	public ItemStack onItemRightClick(ItemStack par1ItemStack, World par2World,
			EntityPlayer player) {
		List<Entity> entitys = par2World.getEntitiesWithinAABB(Entity.class,
				AxisAlignedBB.getBoundingBox(player.posX - 2.,
						player.posY - 2., player.posZ - 2., player.posX + 2.,
						player.posY + .5, player.posZ + 2.));

		for (int i = 0; i < entitys.size(); i++) {
			if (entitys.get(i).getCommandSenderName()
					.equals("entity.EntityItemGoldy.name")) {
				WorldUtil.removeEntity(par2World, entitys.get(i));
				PlayHandler.playAtEntity("GoldyCollect", par2World, player, 1,
						1);
				InventoryUtil.addItemToPlayerInventory(player, Stuff.itemGoldy);
			}
			if (entitys.get(i).getCommandSenderName()
					.equals("entity.EntityItemUpgrade.name")) {
				WorldUtil.removeEntity(par2World, entitys.get(i));
				PlayHandler.playAtEntity("UpgradeCollect", par2World, player,
						1, 1);
				InventoryUtil.addItemToPlayerInventory(player,
						Stuff.itemUpgrade);
			}
		}
		return super.onItemRightClick(par1ItemStack, par2World, player);
	}
}
