package mattsmc.bittrip.block.model;

import net.minecraft.client.model.*;
import net.minecraft.entity.Entity;

public class ModelCrystal extends ModelBase {
    ModelRenderer base;
    ModelRenderer crys1;
    ModelRenderer crys2;
    ModelRenderer crys3;
    ModelRenderer crys4;
    ModelRenderer crys5;
    ModelRenderer crys1top;
    ModelRenderer crys2top;
    ModelRenderer crys4top;
    ModelRenderer crys3top;
    ModelRenderer crys3toptop;
    ModelRenderer crys4toptop;
    
    public ModelCrystal() {
        textureWidth = 64;
        textureHeight = 256;
        
        base = new ModelRenderer(this, 0, 0);
        base.addBox(0F, 0F, 0F, 12, 2, 12);
        base.setRotationPoint(-6F, 22F, -6F);
        base.setTextureSize(64, 256);
        base.mirror = true;
        setRotation(base, 0F, 0F, 0F);
        crys1 = new ModelRenderer(this, 36, 14);
        crys1.addBox(-2F, -9F, -2F, 6, 10, 4);
        crys1.setRotationPoint(-2F, 22F, -3F);
        crys1.setTextureSize(64, 256);
        crys1.mirror = true;
        setRotation(crys1, 0.1487144F, 0F, 0F);
        crys2 = new ModelRenderer(this, 16, 14);
        crys2.addBox(-2F, -10.13333F, -2F, 6, 10, 4);
        crys2.setRotationPoint(0F, 23F, 3F);
        crys2.setTextureSize(64, 256);
        crys2.mirror = true;
        setRotation(crys2, -0.1487195F, 0F, 0F);
        crys3 = new ModelRenderer(this, 0, 28);
        crys3.addBox(-3F, -10F, -2F, 4, 10, 4);
        crys3.setRotationPoint(-2F, 22F, 0F);
        crys3.setTextureSize(64, 256);
        crys3.mirror = true;
        setRotation(crys3, -0.0743572F, 0F, -0.2602503F);
        crys4 = new ModelRenderer(this, 0, 14);
        crys4.addBox(-2F, -10F, -2F, 4, 10, 4);
        crys4.setRotationPoint(3F, 22F, 0F);
        crys4.setTextureSize(64, 256);
        crys4.mirror = true;
        setRotation(crys4, 0.1487206F, 0F, 0.260246F);
        crys5 = new ModelRenderer(this, 0, 14);
        crys5.addBox(-3F, -10F, -2F, 4, 10, 4);
        crys5.setRotationPoint(-2F, 22F, 0F);
        crys5.setTextureSize(64, 256);
        crys5.mirror = true;
        setRotation(crys5, -0.0743572F, 0F, -0.2602503F);
        crys1top = new ModelRenderer(this, 0, 42);
        crys1top.addBox(-2.866667F, -2F, 0F, 4, 3, 2);
        crys1top.setRotationPoint(0F, 12.33333F, -5.133333F);
        crys1top.setTextureSize(64, 256);
        crys1top.mirror = true;
        setRotation(crys1top, 0.1115358F, 0F, 0F);
        crys2top = new ModelRenderer(this, 12, 42);
        crys2top.addBox(-0.06666667F, -1.8F, -1F, 4, 3, 2);
        crys2top.setRotationPoint(-1F, 11.86667F, 4.733333F);
        crys2top.setTextureSize(64, 256);
        crys2top.mirror = true;
        setRotation(crys2top, -0.0743572F, 0F, 0F);
        crys4top = new ModelRenderer(this, 24, 29);
        crys4top.addBox(0F, 0F, -1F, 2, 4, 2);
        crys4top.setRotationPoint(4.533333F, 9.6F, -1.666667F);
        crys4top.setTextureSize(64, 256);
        crys4top.mirror = true;
        setRotation(crys4top, 0.1115358F, 0F, 0F);
        crys3top = new ModelRenderer(this, 16, 29);
        crys3top.addBox(0F, 0F, 0F, 2, 3, 2);
        crys3top.setRotationPoint(-6.666667F, 10.2F, -0.4F);
        crys3top.setTextureSize(64, 256);
        crys3top.mirror = true;
        setRotation(crys3top, 0.0371786F, 0F, 0F);
        crys3toptop = new ModelRenderer(this, 32, 29);
        crys3toptop.addBox(0F, 0F, 0F, 1, 2, 1);
        crys3toptop.setRotationPoint(-6F, 8.733334F, 0.2F);
        crys3toptop.setTextureSize(64, 256);
        crys3toptop.mirror = true;
        setRotation(crys3toptop, 0F, 0F, 0F);
        crys4toptop = new ModelRenderer(this, 32, 32);
        crys4toptop.addBox(0F, 0F, 0F, 1, 2, 1);
        crys4toptop.setRotationPoint(5F, 8F, -2F);
        crys4toptop.setTextureSize(64, 256);
        crys4toptop.mirror = true;
        setRotation(crys4toptop, 0F, 0F, 0F);
    }
    
    public void renderBase(float f5) {
        base.render(f5);
    }
    public void renderModel(float f5) {
        crys1.render(f5);
        crys2.render(f5);
        crys3.render(f5);
        crys4.render(f5);
        crys5.render(f5);
        crys1top.render(f5);
        crys2top.render(f5);
        crys4top.render(f5);
        crys3top.render(f5);
        crys3toptop.render(f5);
        crys4toptop.render(f5);
    }
    
    public void render(Entity entity, float f, float f1, float f2, float f3,
            float f4, float f5) {
        super.render(entity, f, f1, f2, f3, f4, f5);
        setRotationAngles(f, f1, f2, f3, f4, f5, entity);
        base.render(f5);
        crys1.render(f5);
        crys2.render(f5);
        crys3.render(f5);
        crys4.render(f5);
        crys5.render(f5);
        crys1top.render(f5);
        crys2top.render(f5);
        crys4top.render(f5);
        crys3top.render(f5);
        crys3toptop.render(f5);
        crys4toptop.render(f5);
    }
    
    private void setRotation(ModelRenderer model, float x, float y, float z) {
        model.rotateAngleX = x;
        model.rotateAngleY = y;
        model.rotateAngleZ = z;
    }
    
    public void setRotationAngles(float f, float f1, float f2, float f3,
            float f4, float f5, Entity entity) {
        super.setRotationAngles(f, f1, f2, f3, f4, f5, entity);
    }

    
}
