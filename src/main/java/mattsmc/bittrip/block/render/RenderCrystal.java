package mattsmc.bittrip.block.render;

import mattsmc.bittrip.block.model.ModelCrystal;
import mattsmc.bittrip.lib.Reference;
import net.minecraft.client.renderer.tileentity.TileEntitySpecialRenderer;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.ResourceLocation;

import org.lwjgl.opengl.GL11;

public class RenderCrystal extends TileEntitySpecialRenderer {
    
    private ModelCrystal                  model;
    private static final ResourceLocation crystalTexure = new ResourceLocation(Reference.MODID.toLowerCase(),"textures/model/Crystal.png");
    
    public RenderCrystal() {
        this.model = new ModelCrystal();
    }
    
    public void renderTileEntityAt(TileEntity entity, double x, double y,
            double z, float f) {
        GL11.glPushMatrix(); // {
        GL11.glTranslatef((float) x + 0.5F, (float) y + 1.5F, (float) z + 0.5F); // dunno
        GL11.glRotatef(180F, 0F, 0F, 1F); //rotate
        bindTexture(crystalTexure);
        GL11.glPushMatrix(); // {
        model.renderBase(0.0625F); // Sets render of base
        
        GL11.glEnable(GL11.GL_BLEND);
        GL11.glBlendFunc(GL11.GL_SRC_ALPHA, GL11.GL_ONE_MINUS_SRC_ALPHA);
        GL11.glColor4f(1f, 1f, 1f, 0.90f);

        model.renderModel(0.0625F);

        GL11.glDisable(GL11.GL_BLEND);
        GL11.glColor4f(1f, 1f, 1f, 1f);
        GL11.glPopMatrix(); // }
        GL11.glPopMatrix(); // }
        
    }
    
}
