package mattsmc.bittrip.block.render;

import mattsmc.bittrip.block.model.ModelCrystal;
import mattsmc.bittrip.block.tile.BlockCrystalTile;
import net.minecraft.client.renderer.tileentity.TileEntityRendererDispatcher;
import net.minecraft.item.ItemStack;
import net.minecraftforge.client.IItemRenderer;

public class ItemRenderCrystal implements IItemRenderer {

    private ModelCrystal model;
    
    public ItemRenderCrystal() {
        model = new ModelCrystal();
    }
    
    @Override
    public boolean handleRenderType(ItemStack item, ItemRenderType type) {
        return true;
    }

    @Override
    public boolean shouldUseRenderHelper(ItemRenderType type, ItemStack item,
            ItemRendererHelper helper) {
        return true;
    }

    @Override
    public void renderItem(ItemRenderType type, ItemStack item, Object... data) {
       TileEntityRendererDispatcher.instance.renderTileEntityAt(new BlockCrystalTile(), 0.0D, 0.0D, 0.0D, 0.0F);
    }
    
}
