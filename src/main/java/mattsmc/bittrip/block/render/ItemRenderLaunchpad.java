package mattsmc.bittrip.block.render;

import mattsmc.bittrip.block.model.ModelLaunchpad;
import mattsmc.bittrip.block.tile.BlockLaunchpadTile;
import net.minecraft.client.renderer.tileentity.TileEntityRendererDispatcher;
import net.minecraft.item.ItemStack;
import net.minecraftforge.client.IItemRenderer;

public class ItemRenderLaunchpad implements IItemRenderer {

    ModelLaunchpad model;
    
    public ItemRenderLaunchpad() {
        model = new ModelLaunchpad();
    }
    
    @Override
    public boolean handleRenderType(ItemStack item, ItemRenderType type) {
        return true;
    }

    @Override
    public boolean shouldUseRenderHelper(ItemRenderType type, ItemStack item,
            ItemRendererHelper helper) {
        return true;
    }

    @Override
    public void renderItem(ItemRenderType type, ItemStack item, Object... data) {
       TileEntityRendererDispatcher.instance.renderTileEntityAt(new BlockLaunchpadTile(), 0.0D, 0.0D, 0.0D, 0.0F);
    }
    
    
}
