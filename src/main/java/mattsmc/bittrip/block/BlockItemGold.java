package mattsmc.bittrip.block;

import org.apache.logging.log4j.Level;

import mattsmc.bittrip.common.Stuff;
import mattsmc.bittrip.item.entity.EntityItemGoldy;
import mattsmc.bittrip.lib.Reference;
import mattsmc.bittrip.util.BlockUtils;
import mattsmc.bittrip.util.ItemUtils;
import mattsmc.bittrip.util.LogHelper;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Blocks;
import net.minecraft.item.ItemStack;
import net.minecraft.util.IIcon;
import net.minecraft.world.World;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

public class BlockItemGold extends Block {
    
    private IIcon[] icons;
    
    public BlockItemGold(int i) {
        super(Material.rock);
        this.setBlockName("blockItemGold");
        this.setCreativeTab(Stuff.BitTrippingTab);
        icons = new IIcon[6];
    }
    
    @Override
    @SideOnly(Side.CLIENT)
    public void registerBlockIcons(IIconRegister iconRegister) {
        for (int i = 0; i < icons.length; i++) {
            String name;
            switch (i) {
                case 0:
                    name = "0";
                    break;
                case 1:
                    name = "1";
                    break;
                case 2:
                    name = "2";
                    break;
                case 3:
                    name = "3";
                    break;
                case 4:
                    name = "4";
                    break;
                case 5:
                    name = "5";
                    break;
                default:
                    name = "0";
            }
            icons[i] = iconRegister.registerIcon(Reference.MODID + ":BlockItemGold/"
                    + super.getUnlocalizedName() + name);
        }
    }
    
    @Override
    @SideOnly(Side.CLIENT)
    public IIcon getIcon(int side, int meta) {
        if (side <= 5) return icons[side];
        else return icons[0];
    }
    
    @Override
    public boolean onBlockActivated(World world, int x, int y, int z,
            EntityPlayer player, int p_149727_6_, float p_149727_7_,
            float p_149727_8_, float p_149727_9_) {
        
       
        
        return super.onBlockActivated(world, x, y, z, player, p_149727_6_,
                p_149727_7_, p_149727_8_, p_149727_9_);
    }
    
}
