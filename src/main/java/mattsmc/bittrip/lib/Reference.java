package mattsmc.bittrip.lib;

public class Reference {
	// This is the referencing file for the modification Bit.Trip Runner
	/**
	 * The Mod-ID of the Mod
	 */
	public static final String MODID = "BitTripRunner";
	/**
	 * The Current Version of the mod
	 */
	public static final String VERSION = "Closed Alpha 0.4";
	/**
	 * The name used IG of the mod
	 */
	public static final String NAME = "Bit.Trip Runner Mod";
	/**
	 * The version of the supported client
	 */
	public static final String MCVERSION = "MC172";
	/**
	 * URL to check for updates
	 */
	public static final String VERSIONCHECKURL = "https://dl.dropboxusercontent.com/s/jy8w3a6nkxhzbhf/VersionCheck.xml?dl=1&token_hash=AAEcI7G0cDFQOqnH29gCCauq5vA_2y35StjMFsa_RnQ4OA";
	/**
	 * URL where user gets sent to update
	 */
	public static final String UPDATEURL = "https://www.tinyurl.com/MattsMods";

}
