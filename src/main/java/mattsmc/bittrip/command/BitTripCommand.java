package mattsmc.bittrip.command;

import java.util.List;

import mattsmc.bittrip.lib.Reference;
import mattsmc.bittrip.util.RandomUtil;
import mattsmc.bittrip.util.client.PlayHandler;
import mattsmc.bittrip.util.client.XMLParser;
import net.minecraft.command.CommandBase;
import net.minecraft.command.ICommand;
import net.minecraft.command.ICommandSender;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.world.World;

public class BitTripCommand extends CommandBase implements ICommand  {
    
    @Override
    public int compareTo(Object o) {
        return 0;
    }
    
    @Override
    public String getCommandName() {
        return "bittrip";
    }
    
    @Override
    public String getCommandUsage(ICommandSender var1) {
        return "/bittrip <checkversion | clientspecials>";
    }
    
    @Override
    public List getCommandAliases() {
        return null;
    }
    
    @Override
    public void processCommand(ICommandSender sender, String[] args) {
        if (args != null && args.length > 0 && args[0] != null) {
            if (args[0].equalsIgnoreCase("checkversion")) {
                XMLParser.checkUpdate(sender, true, false,
                        Reference.VERSIONCHECKURL, Reference.MCVERSION,
                        Reference.MODID, Reference.VERSION, Reference.UPDATEURL);
            }
            else if (args[0].equalsIgnoreCase("clientspecials")) {
                if (((EntityPlayer) sender).getDisplayName().equals(
                        "JustKayneNL")) {
                    PlayHandler.playAtEntity("MLPTS",
                            (World) sender.getEntityWorld(),
                            (EntityPlayer) sender, 1, 1);
                }
                else {

                }
            }
        }
    }
    
    @Override
    public boolean canCommandSenderUseCommand(ICommandSender var1) {
        return true;
    }
    
    @Override
    public List addTabCompletionOptions(ICommandSender var1, String[] var2) {
        return null;
    }
    
    @Override
    public boolean isUsernameIndex(String[] var1, int var2) {
        return false;
    }
    
}
