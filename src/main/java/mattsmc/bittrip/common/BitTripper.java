package mattsmc.bittrip.common;

import mattsmc.bittrip.command.BitTripCommand;
import mattsmc.bittrip.event.EventListener;
import mattsmc.bittrip.lib.Reference;
import mattsmc.bittrip.proxy.CommonProxy;
import mattsmc.bittrip.util.LogHelper;
import mattsmc.bittrip.util.client.XMLParser;
import net.minecraft.init.Items;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagList;
import net.minecraft.nbt.NBTTagString;
import net.minecraft.util.StatCollector;
import cpw.mods.fml.common.FMLCommonHandler;
import cpw.mods.fml.common.Mod;
import cpw.mods.fml.common.Mod.EventHandler;
import cpw.mods.fml.common.Mod.Instance;
import cpw.mods.fml.common.SidedProxy;
import cpw.mods.fml.common.event.FMLInitializationEvent;
import cpw.mods.fml.common.event.FMLPostInitializationEvent;
import cpw.mods.fml.common.event.FMLPreInitializationEvent;
import cpw.mods.fml.common.event.FMLServerStartingEvent;

@Mod(modid = Reference.MODID, version = Reference.VERSION, name = Reference.NAME)
public class BitTripper {
	@Instance(Reference.MODID)
	public static BitTripper instance;

	@SidedProxy(clientSide = "mattsmc.bittrip.proxy.ClientProxy", serverSide = "mattsmc.bittrip.proxy.CommonProxy")
	public static CommonProxy proxy;

	public static boolean isParkourActive;
	
	@EventHandler
	public void preInit(FMLPreInitializationEvent event) {
		// Load EventHandler Class
		FMLCommonHandler.instance().bus().register(new EventListener());

		// Load Items,Blocks and CreativeTab
		Stuff.load();

		// Load ClientRendering
		proxy.renderInformation();
		
		isParkourActive = false;
	}

	@EventHandler
	public void init(FMLInitializationEvent event) {
		LogHelper.init();
	}

	@EventHandler
	public void serverLoading(FMLServerStartingEvent event) {
		event.registerServerCommand(new BitTripCommand());
	}

	@EventHandler
	public void postInit(FMLPostInitializationEvent event) {
		XMLParser.checkUpdate(null, false, true, Reference.VERSIONCHECKURL,
				Reference.MCVERSION, Reference.MODID, Reference.VERSION, Reference.UPDATEURL);
	}
}
