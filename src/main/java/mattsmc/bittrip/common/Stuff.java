package mattsmc.bittrip.common;

import mattsmc.bittrip.block.BlockCrystal;
import mattsmc.bittrip.block.BlockItemGold;
import mattsmc.bittrip.block.BlockLaunchpad;
import mattsmc.bittrip.block.BlockStarter;
import mattsmc.bittrip.block.BlockUpgradeBlock;
import mattsmc.bittrip.block.tile.BlockCrystalTile;
import mattsmc.bittrip.block.tile.BlockLaunchpadTile;
import mattsmc.bittrip.item.ItemActivator;
import mattsmc.bittrip.item.ItemGoldy;
import mattsmc.bittrip.item.ItemRangeRemover;
import mattsmc.bittrip.item.ItemStarter;
import mattsmc.bittrip.item.ItemUpgrade;
import mattsmc.bittrip.item.entity.EntityItemGoldy;
import mattsmc.bittrip.item.entity.EntityItemUpgrade;
import mattsmc.bittrip.item.record.RecordBronyCD;
import mattsmc.bittrip.item.record.RecordThemeSong;
import mattsmc.bittrip.lib.Reference;
import mattsmc.bittrip.util.RegistryHelper;
import net.minecraft.block.Block;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.init.Items;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import cpw.mods.fml.common.registry.GameRegistry;

public class Stuff {
	
	// Defining Stuff
	public static CreativeTabs BitTrippingTab;

	public static Item BitTripperTabPlaceHolder;
	public static Item itemGoldy;
	public static Item itemUpgrade;
	public static Item itemRangeRemover;
	public static Item itemActivator;
	public static Item itemStarter;
	
	public static Item recordBronyCD;
	public static Item recordThemeSong;

	public static Block blockItemGold;
	public static Block blockUpgradeBlock;
	public static Block blockCrystal;
	public static Block blockLaunchpad;
	public static Block blockStarter;

	/**
	 * Loads all the Items/Blocks/CraftingRecipes/Entities/TileEntitys into the game
	 */
	public static void load() {
		//Initializing Stuffs
		BitTripperTabPlaceHolder = new Item().setTextureName(
				Reference.MODID + ":itemBitTripperTabPlaceholder")
				.setUnlocalizedName("itemBitTripperTabPlaceholder");

		BitTrippingTab = new CreativeTabs("BitTripperTab") {
			public Item getTabIconItem() {
				return Stuff.BitTripperTabPlaceHolder;
			}
		}.setNoTitle().setBackgroundImageName("inventory.png");

		blockCrystal = new BlockCrystal();
		blockLaunchpad = new BlockLaunchpad();
		blockItemGold = new BlockItemGold(3000);
		blockUpgradeBlock = new BlockUpgradeBlock(3001);
		blockStarter = new BlockStarter();

		itemUpgrade = new ItemUpgrade();
		itemGoldy = new ItemGoldy();
		itemRangeRemover = new ItemRangeRemover();
		itemActivator = new ItemActivator();
		itemStarter = new ItemStarter();
		
		recordBronyCD = new RecordBronyCD();
		recordThemeSong = new RecordThemeSong();
		
		

		// Registering Stuff
		RegistryHelper.registerItem(BitTripperTabPlaceHolder);

		RegistryHelper.registerItem(itemGoldy);
		RegistryHelper.registerItem(itemUpgrade);
		RegistryHelper.registerItem(itemRangeRemover);
		RegistryHelper.registerItem(itemActivator);
		RegistryHelper.registerItem(itemStarter);
		
		RegistryHelper.registerItem(recordBronyCD);
		RegistryHelper.registerItem(recordThemeSong);

		RegistryHelper.registerBlock(blockCrystal);
		RegistryHelper.registerBlock(blockLaunchpad);
		RegistryHelper.registerBlock(blockItemGold);
		RegistryHelper.registerBlock(blockUpgradeBlock);
		RegistryHelper.registerBlock(blockStarter);
		
		RegistryHelper.registerTileEntity(BlockCrystalTile.class);
		RegistryHelper.registerTileEntity(BlockLaunchpadTile.class);

		RegistryHelper.registerEntity(EntityItemGoldy.class, "EntityItemGoldy",
				1, 528);

		RegistryHelper.registerEntity(EntityItemUpgrade.class,
				"EntityItemUpgrade", 2, 528);

		GameRegistry.addRecipe(new ItemStack(itemStarter, 1), new Object[] { "X  ", "   ", "   ", 'X', Items.apple});
		
	}
}
