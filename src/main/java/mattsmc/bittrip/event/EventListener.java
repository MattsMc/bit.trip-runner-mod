package mattsmc.bittrip.event;

import mattsmc.bittrip.common.Stuff;
import mattsmc.bittrip.exception.NotLikedException;
import mattsmc.bittrip.lib.Reference;
import mattsmc.bittrip.util.client.InventoryUtil;
import mattsmc.bittrip.util.client.PlayHandler;
import mattsmc.bittrip.util.client.XMLParser;
import net.minecraft.client.Minecraft;
import net.minecraft.init.Items;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagList;
import net.minecraft.nbt.NBTTagString;
import net.minecraft.util.StatCollector;
import cpw.mods.fml.common.eventhandler.SubscribeEvent;
import cpw.mods.fml.common.gameevent.PlayerEvent.PlayerLoggedInEvent;

public class EventListener {
	@SubscribeEvent
	public void onPlayerLogin(PlayerLoggedInEvent event) {
		XMLParser.checkUpdate(event.player, false, false,
				Reference.VERSIONCHECKURL, Reference.MCVERSION,
				Reference.MODID, Reference.VERSION, Reference.UPDATEURL);

		if (event.player.getDisplayName().equals("JustKayneNL")) {
			PlayHandler.playAtEntity("MLPTS", event.player.worldObj,
					event.player, 1, 1);
		}
		
		event.player.inventory.addItemStackToInventory(manual);		
	}
	 public static ItemStack manual = new ItemStack(Items.written_book);
	    static {
	        manual.setTagInfo("author", new NBTTagString("cpw"));
	        manual.setTagInfo("title", new NBTTagString(StatCollector.translateToLocal("book.ironchest:dirtchest9000.title")));
	        NBTTagList pages = new NBTTagList();
	        pages.appendTag(new NBTTagString(StatCollector.translateToLocal("book.ironchest:dirtchest9000.page1")));
	        pages.appendTag(new NBTTagString(StatCollector.translateToLocal("book.ironchest:dirtchest9000.page2")));
	        pages.appendTag(new NBTTagString(StatCollector.translateToLocal("book.ironchest:dirtchest9000.page3")));
	        pages.appendTag(new NBTTagString(StatCollector.translateToLocal("book.ironchest:dirtchest9000.page4")));
	        pages.appendTag(new NBTTagString(StatCollector.translateToLocal("book.ironchest:dirtchest9000.page5")));
	        manual.setTagInfo("pages", pages);
	    }
}
