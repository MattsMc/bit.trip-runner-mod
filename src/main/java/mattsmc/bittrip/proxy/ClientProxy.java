package mattsmc.bittrip.proxy;

import mattsmc.bittrip.block.render.ItemRenderCrystal;
import mattsmc.bittrip.block.render.ItemRenderLaunchpad;
import mattsmc.bittrip.block.render.RenderCrystal;
import mattsmc.bittrip.block.render.RenderLaunchpad;
import mattsmc.bittrip.block.tile.BlockCrystalTile;
import mattsmc.bittrip.block.tile.BlockLaunchpadTile;
import mattsmc.bittrip.common.Stuff;
import mattsmc.bittrip.item.entity.EntityItemGoldy;
import mattsmc.bittrip.item.entity.EntityItemUpgrade;
import mattsmc.bittrip.item.entity.render.RenderItemGoldy;
import mattsmc.bittrip.item.entity.render.RenderItemUpgrade;
import mattsmc.bittrip.util.client.ClientRegistryHelper;
import cpw.mods.fml.client.registry.ClientRegistry;
import cpw.mods.fml.client.registry.RenderingRegistry;

public class ClientProxy extends CommonProxy {
    
    @Override
    public void renderInformation() {
        ClientRegistry.bindTileEntitySpecialRenderer(BlockCrystalTile.class, new RenderCrystal());
        ClientRegistry.bindTileEntitySpecialRenderer(BlockLaunchpadTile.class, new RenderLaunchpad());
        
        ClientRegistryHelper.registerItemRenderer(Stuff.blockCrystal, new ItemRenderCrystal());
        ClientRegistryHelper.registerItemRenderer(Stuff.blockLaunchpad, new ItemRenderLaunchpad());
        
        RenderingRegistry.registerEntityRenderingHandler(EntityItemGoldy.class, new RenderItemGoldy());
        RenderingRegistry.registerEntityRenderingHandler(EntityItemUpgrade.class, new RenderItemUpgrade());
        
    }
    
 
    
}
